from enum import Enum, auto
from i3ipc import Connection, Con as Container, WindowEvent


class Position(Enum):
    TOP_LEFT = auto()
    TOP_RIGHT = auto()
    BOTTOM_LEFT = auto()
    BOTTOM_RIGHT = auto()


tagged = {}
default_position = Position.BOTTOM_RIGHT


def show_rect(rect):
    print("Rect {", rect.x, rect.y, rect.width, rect.height, "}")


def show_container(con):
    def con_str(con):
        return f"{con.type} {con.name}"

    if isinstance(con, Container):
        print(con_str(con))
    elif isinstance(con, list):
        print([con_str(c) for c in con])
    else:
        print(con)


def show_window_event(conn, e):
    container = e.container
    print(e.change, container.type, container.floating)


def on_window_close(conn, e):
    assert e.change == "close"
    container = e.container

    if container.type != "floating_con" or container.id not in tagged:
        return

    tagged.pop(container.id)


def show_rects(container):
    print("===")
    show_rect(container.rect)
    show_rect(container.window_rect)
    show_rect(container.deco_rect)
    show_rect(container.geometry)
    print("---")


def on_window_floating(conn: Connection, ev: WindowEvent):
    assert ev.change == "floating"

    container = ev.container
    container = conn.get_tree().find_by_id(container.id)
    move_container_to_corner(container, default_position)

    _ws = container.workspace()
    tagged.setdefault(container.id, {})


def on_workspace_focus(conn, e):
    assert e.change == "focus"
    assert e.old is not None

    tree = conn.get_tree()
    curr = e.current

    for k, v in tagged.items():
        if len(v) == 0 or curr.id in v:
            comm = f'move container to workspace "{curr.name}"'
            con = tree.find_by_id(k)
            con.command(comm)


def move_container_to_corner(container: Container, corner: Position):
    # bar-0 dock top
    # Rect { 0 0 1366 768 }
    # Rect { 981 441 375 212 }
    # bar-0 hide top
    # Rect { 0 27 1366 741 }
    # Rect { 981 495 375 212 }
    # assert container.type == "floating_con"

    ws = container.workspace()

    # Dock and Hide
    offset = 27 if ws.rect.x == 0 and ws.rect.y == 0 else ws.rect.y
    my = ws.rect.height - container.rect.height + ws.rect.y

    match corner:
        case Position.TOP_LEFT:
            x, y = 0, 0
        case Position.TOP_RIGHT:
            x, y = ws.rect.width - container.rect.width, 0
        case Position.BOTTOM_LEFT:
            x, y = 0, my
        case Position.BOTTOM_RIGHT:
            x, y = ws.rect.width - container.rect.width, my

    # Without absolute it messes up for bottom-left and bottom-right when bar is visible.
    container.command(f'move absolute position "{x}" "{y}"')


def on_barconfig_update(conn, e):
    assert e.position == "top"
    assert e.mode in {"dock", "hide"}

    for container_id, _ in tagged.items():
        container = conn.get_tree().find_by_id(container_id)
        move_container_to_corner(container, default_position)


def tag_focused(conn, ws=None):
    tree = conn.get_tree()
    container = tree.find_focused()

    if ws is None:
        tagged[container.id] = set()
    else:
        tagged.setdefault(container.id, {})
        tagged[container.id].add(ws)


def main():
    conn = Connection()

    conn.on("barconfig_update", on_barconfig_update)
    conn.on("window::floating", on_window_floating)
    conn.on("window::close", on_window_close)
    conn.on("workspace::focus", on_workspace_focus)

    conn.main()


if __name__ == "__main__":
    raise SystemExit(main())
