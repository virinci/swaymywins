# swaymywins

A tiny tool for controlling windows on i3 and Sway.

I mainly use it for pinning floating windows so they follow me on each workspace. It is very convenient for watching videos while doing something else.

## Instructions (for Fish shell)

```
$ uv venv
$ source ./.venv/bin/activate.fish
$ uv pip compile pyproject.toml -o requirements.txt
$ uv pip sync requirements.txt
```

```
$ source ./venv/bin/activate.fish && python main.py && deactivate
```
